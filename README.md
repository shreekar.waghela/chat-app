# Chat App

This is a web app, a clone of slack.
Here you can create channels where people can send messages to others with access to that channel.

Following are images of chat app:

(NOTE:- Images do not include connection to database)

## Login page
![Login page](./loginpage.png)
## Channel Page
![Channel page](./channelpage.png)