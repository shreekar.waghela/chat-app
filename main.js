let channelUrl = "http://localhost:5000/channels/";
let messageUrl = "http://localhost:5000/messages/";

/*Click event for add channel*/
function addChannelToDb() {
    let channelName = document.getElementById("inputChannelName").value;

    fetch(channelUrl, {
        method: 'post',
        body: JSON.stringify({
            name: channelName
        })
    })

    fetch(channelUrl)
        .then((res) => res.json())
        .then((data) => {
            let result = ""
            let channelInfoArray = data.resources
            channelInfoArray.forEach((val) => {
                const { id, name } = val
                result += `<ul><li>${name}</li></ul>`
            })
            document.getElementById("channelNames").innerHTML = result
        })
}

let channel_name = {};
/*Fetch for showing channel names on screen*/
function showChannelToScreen() {
    fetch(channelUrl)
        .then((res) => res.json())
        .then((data) => {
            let result = ""
            let channelInfoArray = data.resources
            channelInfoArray.forEach((val) => {
                const { id, name } = val
                channel_name[id] = name
                result += `<ul><li id = ${id} onclick = "openChannel(${id})"><a href = "#">${name}</a></li></ul>`
            })
            document.getElementById("channelNames").innerHTML = result
        })
}
showChannelToScreen();

var validName = sessionStorage.getItem("validName")
var currentChannelId = {}

function addUsername(){   
    userName = document.getElementById("usernameText").value;
    sessionStorage.setItem("validName", userName);
}

function openChannel(channelId) {

    currentChannelId.id = channelId
    fetch("http://localhost:5000/messages/?channel_id=" + channelId)
        .then((res) => res.json())
        .then((messageData) => {
            let result = ""
            let messageInfoArray = messageData.resources
            messageInfoArray.forEach((val) => {
                const { username, text, channel_id } = val
                result += `<p><b>${username}:</b> ${text}</p>`
            })
            document.getElementById("messages").innerHTML = result;
            document.getElementById("channelName").innerHTML = channel_name[channelId];
        })
}

/*Sending message to db */
function sendMessageToDb() {
    let message = document.getElementById("messageInput").value

    fetch(messageUrl, {
        method: 'post',
        body: JSON.stringify({
            username: validName,
            text: message,
            channel_id: currentChannelId.id
        })
    })
    .then(() => {
        fetch("http://localhost:5000/broadcast", {
            method: 'post',
            body: JSON.stringify({
                username: validName,
                text: message,
                channel_id: currentChannelId.id
            })
        })
    })
    document.getElementById("sendMessage").reset()
}

const appendMessage = (data) =>{ 
    let messages = document.getElementById('messages');
    let newMessage = `<p><b>${data.username}:</b> ${data.text}</p>`
    messages.innerHTML += newMessage;
}